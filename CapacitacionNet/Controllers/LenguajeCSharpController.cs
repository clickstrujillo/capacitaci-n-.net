﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CapacitacionNet.Controllers
{
    public class LenguajeCSharpController : Controller
    {
        //
        // GET: /LenguajeCSharp/

        public ActionResult Index()
        {
            //Declaracion de variables

            string Cadena = "hola";  //string 
            const double pi = 3.1415; //double
            int entero = 154;    //entero
            decimal Ndecimal = 12.36M;

            Console.Write("Cadena:" + Cadena + " " + " Double:" + pi + " " + " Entero:" + entero + " " + "Decimal:" + Ndecimal);


            //Estructuras de Control

            //if
            int x = 100;
            if (x == 100)
            {
                string structuraif = "X is equal to 100";
                Console.Write("Structura If:" + structuraif);
                //                ViewBag.StrucIf = structuraif;
            }


            //switch
            int enteros = 2;
            switch (enteros)
            {
                case 1: Console.Write("1");
                    break;

                case 2: Console.Write("2");
                    break;

                case 3: Console.Write("3");
                    break;

                default: Console.Write("No exite esta OPcion");
                    break;
            }

            //For

            int Acumulador = 0;
            for (int i = 0; i < 5; i++)
            {
                Acumulador = Acumulador + i;
            }
            Console.Write("Acumulado:" + Acumulador);
            //ViewBag.acumulador = Acumulador;


            //while

            bool condicion = false;
            while (condicion == true)
            {
                //ViewBag.condicion = condicion;
                Console.Write("Cumplio la Condicion");
            }

            Console.Write("No Cumplio la Condicion");


            //do-while
            bool condicion01 = false;
            do
            {
                Console.Write("Cumplio la Condicion:" + condicion01);
                // ViewBag.condicion01 = "no cumplio la condicion";
            } while (condicion == true);
            Console.Write("No Cumplio la Condicion");
            //ViewBag.condicion01 = "Si cumplio la condicion";

            //arreglos

            char[] vocales = new char[5] { 'a', 'e', 'i', 'o', 'u' };
            for (int i = 0; i < vocales.Length; i++)
            {
                Console.Write(vocales[i]);
            }

            return View();
        }

    }
}
