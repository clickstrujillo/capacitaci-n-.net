﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CapacitacionNet.Models;

namespace CapacitacionNet.Controllers
{
    
    public class ProductosController : ApiController
    {

        //public Productos[] Get()
        //{
        //    return new Productos[]
        //    {
        //        new Productos() { Id = 1, nombre = "Tomato Soup", Categoria = "Groceries", Precio = 1 },
        //        new Productos() { Id = 2, nombre = "Yo-yo", Categoria = "Toys", Precio = 3.75M },
        //        new Productos() { Id = 3, nombre = "Hammer", Categoria = "Hardware", Precio = 16.99M },
        //        new Productos() { Id = 4, nombre = "Monitor", Categoria = "Hardware", Precio = 520.99M }
        //    };
        //}

        
        private static List<Productos> _ProductosLits = new List<Productos>()
        {
                new Productos() { Id = 1, nombre = "Tomato Soup", Categoria = "Groceries", Precio = 1 },
                new Productos() { Id = 2, nombre = "Yo-yo", Categoria = "Toys", Precio = 3.75M },
                new Productos() { Id = 3, nombre = "Hammer", Categoria = "Hardware", Precio = 16.99M },
                new Productos() { Id = 4, nombre = "Monitor", Categoria = "Hardware", Precio = 520.99M }
        };

        // GET api/productos
        
        public IEnumerable<Productos> GetAll()
        {
            return _ProductosLits;
        }

        public Productos GetProductosById(int id)
        {
            var productos = _ProductosLits.SingleOrDefault(c => c.Id == id);
            if(productos==null)
                new ArgumentNullException("Productos fue encontrado");

            return productos;
        }

        //public Productos GetProductosName(string Nombre) {
        //    var productos = _ProductosLits.SingleOrDefault(c => c.nombre.StartsWith(Nombre) || c.Categoria.StartsWith(Nombre));
        //    if (productos == null)
        //        new ArgumentNullException("producto fue encontrado");
        //    return productos;
        //}

        // GET api/productos/5
        //public Productos Get(int id)
        //{
        //    return new Productos() { Id = id, nombre = "tablet", Categoria = "software", Precio = 120.99M };
        //}


        // GET api/productos/5
        //public Productos Get(int id, string nombre, string categoria, decimal precio)
        //{
        //    return new Productos() { Id = id, nombre = nombre, Categoria = categoria, Precio = precio };
        //}


        // POST api/productos
        public void Post([FromBody]Productos value)
        {
        }

        // PUT api/productos/5
        public void Put(int id, [FromBody]Productos value)
        {
        }

        // DELETE api/productos/5
        public void Delete(int id)
        {
        }
    }
}
