﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoDB.Driver;
using CapacitacionNet.Models;

namespace CapacitacionNet.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            MongoClient mc = new MongoClient("mongodb://127.0.0.1");
            MongoServer mongo = mc.GetServer();
            var bbdds = mongo.GetDatabaseNames();
            MongoDatabase db = mongo.GetDatabase("mibasedatos");
            var collection = db.GetCollection<Pelicula>("peliculas");
            ViewBag.listaDatos = collection.FindAll().ToList<Pelicula>();
            return View("index");
        }
    }
}
