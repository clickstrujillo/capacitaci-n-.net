﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapacitacionNet.Models
{
    public class Productos
    {
        public int Id { get; set; }
        public string nombre { get; set; }
        public string Categoria { get; set; }
        public decimal Precio { get; set; }
    }
}