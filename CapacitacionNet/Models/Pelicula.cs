﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;

namespace CapacitacionNet.Models
{
    public class Pelicula
    {
        public ObjectId id { get; set; }
        public string nombre { get; set; }
        public string genero { get; set; }
        public string anio { get; set; }
    }
}